# Assignment2 #

This file describes the script code db_create.py, sql_temp.py, excel_temp.py, and numpy_temp.py

### What is this repository for? ###

The code associated with this readme consists of four pyhton scripts that manipulate (read / write) excel and database files.
With the information of the excel files, the databases are created and from them other excel files are generated.

### How do I get set up? ###

* Summary of set up
To execute the scripts you must have the dependencies installed and follow the order shown below

* Configuration
To be able to execute the scripts must be installed python modules openpyxl, matplotlib and numpy that normally
do not come in the standard installation of python. For a simpler way of installing them you can use pip, which 
is a tool for installing packages in python, you can get pip infromacion at https://pypi.python.org/pypi/pip and can be 
obtained in https : //pip.pypa.io/en/stable/installing/

* Dependencies
python installation
python modules: openpyxl, matplotlib and numpy
excel files: GlobalLandTemperaturesByCountry.xlsx, GlobalLandTemperaturesByMajorCity.xlsx, GlobalLandTemperaturesByState.xlsx

* Database configuration
The database created is an sqlite database, so to access it you must follow the sqlite syntax

* How to run tests
The files must be run in the following order:  db_create.py, sql_temp.py, excel_temp.py, and numpy_temp.py