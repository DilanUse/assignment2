from openpyxl import Workbook
import sqlite3
from openpyxl.chart import (
    LineChart,
    Reference,
)
from openpyxl.chart.axis import DateAxis

wb = Workbook() # create excel file representation
ws = wb.active # obtain sheet
ws.title = "Temperature by city" # change tittle sheet
ws.cell(column=1, row=1, value="Year")
ws.cell(column=2, row=1, value="AverageTemperature")
ws.cell(column=3, row=1, value="Country")

# conect database
con_bd = sqlite3.connect('Temperatures.db') #open database
con_bd.text_factory = str # set textfactory
cursor_bd = con_bd.cursor() #obtain cursor

# validations
cursor_bd.execute("""select count(*) 
                  from sqlite_master 
                  where type='table' 
                  and name = 'ByMajorCity'""")
table =  cursor_bd.fetchall() # obstain registers
if( not table[0][0] ):
    print("The required tables do not exist in the database")
    exit(0)

# extract cities of china
cursor_bd.execute("""SELECT DISTINCT City
                    FROM ByMajorCity
                    WHERE Country = 'China'
                    ;""")

cities =  cursor_bd.fetchall() # obstain cities

sheet_row = 2 # initial row in the sheet to write

for city in cities:
    # extract data years of an city
    cursor_bd.execute("""SELECT DISTINCT strftime("%Y", Date)
                        FROM ByMajorCity
                        WHERE Country = 'China'
                        AND City = (?)
                        ;""", city)

    years =  cursor_bd.fetchall() # obstain years

    for year in years:
        # extract register to database
        cursor_bd.execute("""SELECT AVG(Av_temp)
                            FROM ByMajorCity
                            WHERE City = ?
                            AND strftime("%Y", Date) = ?
                            ;""", (city[0], year[0]))

        avg =  cursor_bd.fetchall() # obstain registers

        # write data in excel sheet
        ws.cell(column=1, row=sheet_row, value=year[0])
        ws.cell(column=2, row=sheet_row, value=avg[0][0])
        ws.cell(column=3, row=sheet_row, value=city[0])
        sheet_row += 1

# create chart in excel sheet
c1 = LineChart()
c1.title = "Average temperature for China cities"
c1.style = 13
c1.y_axis.title = 'Average Temperature'
c1.x_axis.title = 'Samples'
data = Reference(ws, min_col=2, min_row=1, max_col=2, max_row=sheet_row-1)
c1.add_data(data, titles_from_data=True)

# Style the lines
s1 = c1.series[0]
s1.marker.graphicalProperties.solidFill = "FF0000" # Marker filling
s1.marker.graphicalProperties.line.solidFill = "FF0000" # Marker outline

ws.add_chart(c1, "A" + str(sheet_row)) # draw chart

con_bd.close() # close database
wb.save("World Temperature.xlsx") # Save the file