import openpyxl
import sqlite3
import os.path


def getExcelSheet( name, sheet_num ):
    """
    Open an excel file and return a sheet of it
    :param name: name excel file
    :param sheet_num: sheet number in the excel file, starting in one
    :return: sheet in the excel file
    """
    doc = openpyxl.load_workbook(name)
    sheet = doc.get_sheet_by_name(doc.get_sheet_names()[sheet_num - 1])
    return sheet


def printExcelSheet( sheet ):
    """
    output in the console a sheet in an excel file
    :param sheet: sheet to print
    """
    for fila in sheet.rows:
        row = "|"
        for columna in fila:
            row += str(columna.value) + "|"
        print(row)


def insertExcelSheetIntoTable( sheet, cursor_bd, insert_sql ):
    """
    Insert data in a excel sheet into a table
    :param sheet: excel sheet to extract data an insert it into database table
    :param cursor_bd: cursor of database
    :param insert_sql: sql consult to inset data in table
    :return:
    """
    first = 1
    for fila in sheet.rows:
        if (first):
            first = 0
            continue
        params = tuple( [column.value for column in fila] )
        cursor_bd.execute( insert_sql, params)


# initial verifications
if( not os.path.isfile("GlobalLandTemperaturesByCountry.xlsx") or
    not os.path.isfile("GlobalLandTemperaturesByMajorCity.xlsx") or
    not os.path.isfile("GlobalLandTemperaturesByState.xlsx")):
    print("Excel files not exists")
    exit(0)

# conect database
con_bd = sqlite3.connect('Temperatures.db') #open database
con_bd.text_factory = str # set textfactory
cursor_bd = con_bd.cursor() #obtain cursor

# create table tabla
cursor_bd.execute('''CREATE TABLE ByCountry
                    (
                    Date date,
                    Av_temp float(53),
                    Av_temp_uncert float(53),
                    Country varchar(50)
                    );''')

# create table tabla
cursor_bd.execute('''CREATE TABLE ByMajorCity
                    (
                    Date date,
                    Av_temp float(53),
                    Av_temp_uncert float(53),
                    City varchar(50),
                    Country varchar(50),
                    Latitude char(10),
                    Longitude char(10)
                    );''')

# create table tabla
cursor_bd.execute('''CREATE TABLE ByState
                    (
                    Date date,
                    Av_temp float(53),
                    Av_temp_uncert float(53),
                    State varchar(50),
                    Country varchar(50)
                    );''')

# insert into table tabla
consult = '''INSERT INTO tabla
            VALUES (?,?,?,?);
          '''

# obtain excel sheets
sheet_ByCountry = getExcelSheet("GlobalLandTemperaturesByCountry.xlsx", 1) # obtain sheet in the excel file
sheet_ByMajorCity = getExcelSheet("GlobalLandTemperaturesByMajorCity.xlsx", 1) # obtain sheet in the excel file
sheet_ByState = getExcelSheet("GlobalLandTemperaturesByState.xlsx", 1) # obtain sheet in the excel file

# insert data of sheets into tables
insertExcelSheetIntoTable( sheet_ByCountry, cursor_bd, '''INSERT INTO ByCountry
                                                            VALUES (?,?,?,?);
                                                          ''' )
insertExcelSheetIntoTable( sheet_ByMajorCity, cursor_bd, '''INSERT INTO ByMajorCity
                                                            VALUES (?,?,?,?,?,?,?);
                                                          ''' )
insertExcelSheetIntoTable( sheet_ByState, cursor_bd, '''INSERT INTO ByState
                                                        VALUES (?,?,?,?,?);
                                                      ''' )

con_bd.commit() # send data to database
con_bd.close() # close database





