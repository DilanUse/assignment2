import sqlite3


def insertIntoTable( registers, cursor_bd, insert_sql ):
    """
    Insert registers into a table
    :param registers: register to insert into database table
    :param cursor_bd: cursor of database
    :param insert_sql: sql consult to inset data in table
    """
    for reg in registers:
        cursor_bd.execute( insert_sql, reg)


# conect database
con_bd = sqlite3.connect('Temperatures.db') #open database
con_bd.text_factory = str # set textfactory
cursor_bd = con_bd.cursor() #obtain cursor

# validations
cursor_bd.execute("""select count(*) 
                  from sqlite_master 
                  where type='table' 
                  and name = 'ByMajorCity'""")
table1 =  cursor_bd.fetchall() # obstain registers
cursor_bd.execute("""select count(*) 
                  from sqlite_master 
                  where type='table' 
                  and name = 'ByState'""")
table2 =  cursor_bd.fetchall() # obstain registers
if( not table1[0][0] or not table2[0][0] ):
    print("The required tables do not exist in the database")
    exit(0)

# extract registers to database
cursor_bd.execute("""SELECT DISTINCT City, Country, Latitude, Longitude
                    FROM ByMajorCity
                    WHERE Latitude LIKE '%S'
                    ORDER BY Country;""")

registers =  cursor_bd.fetchall() # obstain registers

# create table tabla
cursor_bd.execute('''CREATE TABLE IF NOT EXISTS south_cities
                    (
                    City varchar(50),
                    Country varchar(50),
                    Latitude char(10),
                    Longitude char(10)
                    );''')

# insert into database
insertIntoTable(registers, cursor_bd,  '''INSERT INTO south_cities
                                        VALUES (?,?,?,?);
                                      ''' )

con_bd.commit() # send data to database

# extract register to database
cursor_bd.execute("""SELECT MAX(Av_temp) 
                    FROM ByState
                    WHERE State = 'Queensland'
                    AND Date BETWEEN  '2000-01-01' AND '2000-12-31'
                    ;""")

max_temp =  cursor_bd.fetchall() # obstain registers

# extract register to database
cursor_bd.execute("""SELECT MIN(Av_temp) 
                    FROM ByState
                    WHERE State = 'Queensland'
                    AND Date BETWEEN  '2000-01-01' AND '2000-12-31'
                    ;""")

min_temp =  cursor_bd.fetchall() # obstain registers

# extract register to database
cursor_bd.execute("""SELECT AVG(Av_temp) 
                    FROM ByState
                    WHERE State = 'Queensland'
                    AND Date BETWEEN  '2000-01-01' AND '2000-12-31'
                    ;""")

avg_temp =  cursor_bd.fetchall() # obstain registers

# print the result
print("South cities:")
for reg in registers:
    print(reg)
print("====================================================")
print("Major temperature in Queensland-Australia on year 2000 was " + str(max_temp[0][0]))
print("Less temperature in Queensland-Australia on year 2000 was " + str(min_temp[0][0]))
print("Average temperature in Queensland-Australia on year 2000 was " + str(avg_temp[0][0]))

con_bd.close() # close database

