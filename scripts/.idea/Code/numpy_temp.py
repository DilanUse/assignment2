import openpyxl
import sqlite3
import matplotlib.pyplot as plt
import numpy as np
import os.path

# initial verifications
if( not os.path.isfile("World Temperature.xlsx")):
    print("Excel files do not exists")
    exit(0)

doc = openpyxl.load_workbook("World Temperature.xlsx") # open exel file
sheet = doc.create_sheet("Comparison") # obtain sheet

# give header to sheet
sheet.cell(column=1, row=1, value="Year")
sheet.cell(column=2, row=1, value="State")
sheet.cell(column=3, row=1, value="AvTemperatureState")
sheet.cell(column=4, row=1, value="AvTemperatureAustralia")
sheet.cell(column=5, row=1, value="Difference")

# conect database
con_bd = sqlite3.connect('Temperatures.db') #open database
con_bd.text_factory = str # set textfactory
cursor_bd = con_bd.cursor() #obtain cursor

# validations
cursor_bd.execute("""select count(*) 
                  from sqlite_master 
                  where type='table' 
                  and name = 'ByState'""")
table1 =  cursor_bd.fetchall() # obstain registers
cursor_bd.execute("""select count(*) 
                  from sqlite_master 
                  where type='table' 
                  and name = 'ByCountry'""")
table2 =  cursor_bd.fetchall() # obstain registers
if( not table1[0][0] or not table2[0][0] ):
    print("The required tables do not exist in the database")
    exit(0)

# extract states in Australia
cursor_bd.execute("""SELECT DISTINCT State
                    FROM ByState
                    WHERE Country = 'Australia'
                    ;""")

states =  cursor_bd.fetchall() # obstain cities

# extract data years of Australia
cursor_bd.execute("""SELECT DISTINCT strftime("%Y", Date)
                    FROM ByCountry
                    WHERE Country = 'Australia'
                    ;""")

years_aust = cursor_bd.fetchall()  # obstain years
row_sheet = 2 # row in the sheet
list_difference = [] # lista of comparison

for state in states:
    # extract data years of an state
    cursor_bd.execute("""SELECT DISTINCT strftime("%Y", Date)
                        FROM ByState
                        WHERE Country = 'Australia'
                        AND State = (?)
                        ;""", state)

    years_by_state = cursor_bd.fetchall()  # obstain years

    for year_by_state in years_by_state:
        # extract register to database
        cursor_bd.execute("""SELECT AVG(Av_temp)
                            FROM ByState
                            WHERE State = ?
                            AND strftime("%Y", Date) = ?
                            ;""", (state[0], year_by_state[0]))

        avg_state = cursor_bd.fetchall()  # obstain registers

        # if there is not avg_state in actual year
        if (avg_state[0][0] == None):
            continue

        for year_aust in years_aust:
            if( year_by_state < year_aust ):
                break
            elif( year_by_state > year_aust ):
                continue

            # extract register to database
            cursor_bd.execute("""SELECT AVG(Av_temp)
                                FROM ByCountry
                                WHERE Country = 'Australia'
                                AND strftime("%Y", Date) = ?
                                ;""", year_aust)

            avg_aust = cursor_bd.fetchall()  # obstain registers

            # if there is not avg_aust in actual year
            if(avg_aust[0][0] == None):
                break

            # put data en excel sheet
            sheet.cell(column=1, row=row_sheet, value=year_by_state[0])
            sheet.cell(column=2, row=row_sheet, value=state[0])
            sheet.cell(column=3, row=row_sheet, value=str(avg_state[0][0]))
            sheet.cell(column=4, row=row_sheet, value=str(avg_aust[0][0]))
            sheet.cell(column=5, row=row_sheet, value=str(abs(avg_state[0][0] - avg_aust[0][0])))
            row_sheet += 1
            list_difference.append(abs(avg_state[0][0] - avg_aust[0][0]))


# draw data in a graph
plt.plot(list_difference) # draw data
plt.xlabel("Samples") # tittle axis x
plt.ylabel("Difference") # tittle axis y
plt.title("State Temperatures VS National Temperatures") # graphic tittle
plt.grid(True) # show grid
plt.plot(list_difference, linestyle='-', color='r', label = "Difference") # graphic style
plt.legend() # show legend
plt.show() # show graphic

con_bd.close()  # close database
doc.save("World Temperature.xlsx") # save excel file